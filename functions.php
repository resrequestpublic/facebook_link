<?php

/* 
 * The following code comes from the Facebook developer documentation
 * at the following URL: https://developers.facebook.com/docs/games/canvas/login
 */
function parse_signed_request($signed_request) {
	list($encoded_sig, $payload) = explode('.', $signed_request, 2);

	$config = include('config.php');
	$secret = $config['secret'];

	// decode the data
	$sig = base64_url_decode($encoded_sig);
	$data = json_decode(base64_url_decode($payload), true);

	// confirm the signature
	$expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
	if ($sig !== $expected_sig) {
		error_log('Bad Signed JSON signature!');
		return null;
	}

	return $data;
}

function base64_url_decode($input) {
	return base64_decode(strtr($input, '-_', '+/'));
}

/*
 * Example of the structure sent by a Facebook page. The most important parts 
 * being the id of the page and whether the user is an admin.
 *

		Array
		(
			[algorithm] => HMAC-SHA256
			[issued_at] => 1435045947
			[page] => Array
				(
					[id] => 508226012662610
					[admin] => 1
				)

			[user] => Array
				(
					[country] => za
					[locale] => en_GB
					[age] => Array
						(
							[min] => 21
						)

				)

		)
 */
