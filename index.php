<?php

require_once('functions.php');

// Load the default configuration and any local overrides
$GLOBALS['config'] = include("config.php");
if(file_exists("config.local.php")) {
	$GLOBALS['config'] = array_replace_recursive($GLOBALS['config'], include("config.local.php"));
}

session_start();

if($GLOBALS['config']['authenticate'] && isset($_POST['signed_request'])) {
	session_destroy();
	$_SESSION = array();

	$data = parse_signed_request($_POST['signed_request']);
	if($data === null) {
		die("Access denied");
	}
	session_start();
	$_SESSION['logged_in'] = true;
	$_SESSION['page_id'] = $data['page']['id'];
	if(isset($data['page']['admin']) && $data['page']['admin'] == "1") {
		$_SESSION['admin'] = true;
	} else {
		$_SESSION['admin'] = false;
	}
}

if($GLOBALS['config']['authenticate'] && (!isset($_SESSION['logged_in']) || !$_SESSION['logged_in'])) {
	die("Access denied");
}
?><!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">

<title>Facebook App Template</title>

<meta name="description" content="">
<meta name="author" content="">

<!-- HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
    
<!-- Styles -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<style type="text/css">

body {padding: 20px 0;}

.text-center {
	text-align: center;
}

</style>
    
</head>

<body>

<div class="container canvas">
	<!-- Hero -->
	<section class="page-header">
		<p>Accommodations</p>
	</section>

	<section id="logo">
		<!-- Logo -->
		<div class="text-center">
			<img src="images/logo.png" />
		</div>

		<br />
	</section>

	<section id="camps">
		<h1>Camps</h1>

		<div class="row">
			<div class="span8">
				<h2>Camp 1</h2>
				<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed finibus sapien leo, quis elementum quam feugiat sit amet. Suspendisse non tincidunt neque, vel vulputate quam. In convallis erat nec erat condimentum, id dignissim enim bibendum. Sed eleifend, nibh blandit molestie dignissim, nibh mauris bibendum tortor, et dapibus quam justo vel nulla. Cras mollis nisl ante, sed tristique purus varius pretium. Vivamus varius ante sapien, vitae luctus quam egestas ut. Donec faucibus turpis enim, ultrices fermentum augue placerat et.</span>
			</div>
			<div class="span4">
				<img src="images/placeholder-240x150.png" alt="image goes here" />
			</div>
		</div>

		<div class="row">
			<div class="span8">
				<h2>Camp 2</h2>
				<span>Sed ut leo a nulla rhoncus posuere eu et risus. Maecenas non purus velit. Curabitur tristique tortor arcu, in ultricies metus lobortis molestie. Nam pretium, ipsum at elementum rhoncus, nulla arcu venenatis nibh, vitae vehicula metus nisi in orci. Sed eget posuere tellus. Integer nec odio non massa mattis fringilla. Aliquam erat volutpat. Quisque mauris massa, congue ac diam eget, consectetur sollicitudin urna. Nullam egestas aliquet fermentum. Sed ut mollis urna, vitae fermentum nunc. Vestibulum eget nisl ipsum. Etiam at vehicula metus. Vivamus sit amet est vel arcu congue semper.</span>
			</div>
			<div class="span4">
				<img src="images/placeholder-240x150.png" alt="image goes here" />
			</div>
		</div>

		<div class="row">
			<div class="span8">
				<h2>Camp 3</h2>
				<span>Phasellus commodo vitae justo ut ultrices. Nam justo arcu, malesuada eu viverra non, commodo sit amet diam. Proin consequat lectus vitae pellentesque interdum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi id volutpat nisl. Sed porttitor aliquam nisl eget convallis. Donec eget quam lacus. Nunc eget quam massa. Cras ac orci orci. Phasellus blandit tellus massa, vitae sollicitudin massa cursus at.</span>
			</div>
			<div class="span4">
				<img src="images/placeholder-240x150.png" alt="image goes here" />
			</div>
		</div>

	</section>

	<section id="book">
		<div class="text-center">
			<a href="http://www.resrequest.com" class="btn primary" target="_blank">View Availability & Book Now</a>
		</div>
	</section>

	<footer>
		<p>&copy; ResRequest <?php echo date("Y"); ?></p>
	</footer>

</div>

</body>

</html>
